console.log("hi")

//show posts
const showPosts = (posts) =>{
	let postsEntries = ''

	posts.forEach((post)=>{
		console.log(post);
		postsEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div> 
		`;//to concatenate or link the content of div
	});
	document.querySelector('#div-post-entries').innerHTML = postsEntries;
};
/*fetch method
	syntax: fetch('url', options)
	url - the url which the request is to be made (usually a route to database)
*/

fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data=> showPosts(data));//.then will handle the manipulation

document.querySelector('#form-add-post').addEventListener("submit", (e)=>{
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId:1
		}),
		headers: {
			'Content-Type':'application/json'
		}
	})
	.then(response=>response.json())
	.then(data=>{
		console.log(data);
		alert('Post successfullt added');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
		//to clear the text box after adding
	});
});

//edit posts
const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

//update post
document.querySelector('#form-edit-post').addEventListener("submit", (e)=>{
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/2',{
		method:'PUT',
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId:1
		}),
		headers: {
			'Content-Type':'application/json'
		}
	})
	.then(response=>response.json())
	.then(data=>{
		//response/data are just parameters that we can name freely
		console.log(data);
		alert('Post successfullt added');

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});

//delete-activity
const deletePost = (id)=>{

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method:'DELETE',
		body: JSON.stringify({
			title: document.querySelector(`#post-title-${id}`).value,
			body: document.querySelector(`#post-body-${id}`).value,	
			userId:1
		}),
		headers: {
			'Content-Type':'application/json'
		}
	})
	.then(response=>response.json())
	.then(data=>{
		console.log(data);
		alert('Post successfullt deleted');
		document.querySelector(`#post-${id}`).remove();
		showPosts(data);
	});

}